# Use openjdk image
FROM openjdk:19-jdk-alpine3.16
ARG PROFILE
ARG shellVersion
ARG JDBC
ARG USERNAME
ARG PASSWORD
ENV SPRING_DATASOURCE_URL=$JDBC
ENV SPRING_PROFILES_ACTIVE=$PROFILE
ENV version=$shellVersion 
ENV SPRING_DATASOURCE_USERNAME=$USERNAME
ENV SPRING_DATASOURCE_PASSWORD=$PASSWORD
RUN mkdir /home/app
# RUN adduser -D assignment
WORKDIR  /home/app
COPY ./target/assignment-$version.jar /home/app/
#RUN chown -R assignment:assignment /home/app/
# USER assignment
EXPOSE 8080
CMD java -jar /home/app/assignment-$version.jar --spring.profiles.active=$SPRING_PROFILES_ACTIVE
