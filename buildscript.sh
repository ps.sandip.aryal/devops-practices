#!/bin/bash

# Get the last commit date and hash
commitDate=$(git log -1 --pretty=format:%cd --date=format:%y%m%d)
commitHash=$(git log -1 --abbrev=8 --pretty=format:%h)
# Combine the commit date and hash into a version string
shellVersion="$commitDate-$commitHash"

#Defining Parameters for mysql
user=root
password=Nepal#
# Passing JDBC via environment variable
JDBC=jdbc:mysql://mysqldb:3306/assignment?allowPublicKeyRetrieval=true


echo "Please enter a number 1 for h2 and 2 for sql:"
read num

if [[ $num -eq 1 ]]; then
# Setting the profile to H2
  app_profile="h2"
  # building docker image
  docker build -t $shellVersion --build-arg PROFILE=$app_profile --build-arg shellVersion=$shellVersion .
elif [[ $num -eq 2 ]]; then
# Setting the profile to mysql

  
  #building docker image
  docker build -t pssandiparyal/psartifact:$shellVersion  --build-arg shellVersion=$shellVersion --build-arg JDBC=$JDBC --build-arg USERNAME=$user --build-arg PASSWORD=$password .
else
  echo "Invalid input. Please enter 1 or 2."
fi


# to run docker in sql run following : --network is required as my mysql is in docker isolation
# docker run -p 8080:8080 --network=devops-practices_mysql  230430-bdfceddc:latest


